function Task(strJson) {
	
	this.title = 'Default title';
	this.manager = {};
	this.responsibleEmployee = {};
	this.employeesInvolved = [];
	this.creationDate = new Date();
	this.plannedDate = new Date();
	this.efforts = null;
	this.grade = null;
	this.finishedDate = null;
	var status = 'planned';

	Object.defineProperty(this, 'status', {
		set: function(value) {
			if (statusArr.indexOf(value) >= 0) {
				status = value;
			} else {
				throw 'Invalid input parameter: ' + value + '.';
			}
		},
		get: function () {
			return status;
		},
		enumerable: true
	});

	var statusArr = ['planned', 'inwork', 'finished'];


	var objJson;

	validate(strJson);
	

    try {
        objJson = JSON.parse(strJson);
    } catch(e) {
        throw 'Unable to parse input string as JSON; Error: ' + e;
    }

    for (var prop in objJson) {
        if (prop == 'manager') {
            this.manager = new Employee(JSON.stringify(objJson[prop]));
            continue;
        }

        if (prop == 'responsibleEmployee') {
            this.responsibleEmployee = new Employee(JSON.stringify(objJson[prop]));
            continue;
        }

        if (prop == 'employeesInvolved') {
            for(var emp in objJson[prop]){
                this.employeesInvolved.push(new Employee(JSON.stringify(objJson[prop][deptNum])));
            }
            continue;
        }
        
        else if (objJson.hasOwnProperty(prop) && this.hasOwnProperty(prop)) {
			this[prop] = objJson[prop];
        }
        else {
            console.log("Unable to init property " + prop + ".");
        }
        
    }

}


function validate (str) {
	var flag = false;
	if (typeof str !== 'string') {
		throw 'Invalid input parameter: ' + str;
	}
	flag = true;
	return flag;
}

Task.prototype.toString = function() {
	var strResult = this.__proto__.constructor.name + ': {';
	for (prop in this) {
        if (this.hasOwnProperty(prop)) {
            strResult += ' "' + prop + '": ' + this[prop].toString() + ',';
        }
    }
    strResult = strResult.substr(0,strResult.length-1) + ' }';
    return strResult;
};

function Tasks(argument) {
	
	this.tasksByTitle = [];
	this.tasksByManager = [];

}

Tasks.prototype.createTask = function(first_argument) {};

Tasks.prototype.getTask = function(manager, title) {};



var strTskJSON =  '{"title" : "Задача #1","manager" : {"name" : "Петя"},"responsibleEmployee" : {"position": "Уборщица","department": "Oracle","salary": "38000"},"employeesInvolved" : [{"position": "Уборщица","department": "Oracle","salary": "38000","person": {"name": "Петя","surname": "Петров","dob": "28.01.1990","phone": "+7(288)212-22-33","email": "dd@mail.ru"}},{"position": "Уборщица","department": "Oracle","salary": "38000","person": {"name": "Петя","surname": "Петров","dob": "28.01.1990","phone": "+7(288)212-22-33","email": "dd@mail.ru"}}],"creationDate" : "31.01.2016","plannedDate" : "01.02.2016", "efforts" : "6","grade" : "1","status" : "planned","finishedDate" : ""}'
 