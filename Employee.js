function Employee(init_json) {
	var init_obj = JSON.parse(init_json);
	for (var prop in init_obj){
		if (typeof init_obj[prop] == 'string')
			this[prop] = init_obj[prop];
		else if (prop == 'person'){
			Person.call(this, JSON.stringify(init_obj[prop]));
		}
	}
}
Employee.prototype = Person.prototype;
Employee.prototype.constructor = Employee;
//
var e = new Employee('{ "position": "Уборщица", "department": "Oracle", "salary": "38000", "person": {"name": "Петя", "surname": "Петров", "dob": "28.01.1990", "phone": "+7(288)212-22-33", "email": "dd@mail.ru"}}');