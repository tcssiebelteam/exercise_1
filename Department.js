/**
 * Created by Vadon on 30.01.2016.
 */
function Department(init_json) {
    // Default values, set types for props
    // If 'head' should be a manager, than call new Manager() here - init is based on that...
    this.name = 'Default Department';
    this.head = {name: "Default Manager"};
    this.employees = [];

    //identify input data type - JSON/Object
    var init_obj;
    if (typeof init_json == 'string') {
        console.log('String detected. Parsing as JSON');
        try{
            init_obj = JSON.parse(init_json);
        } catch(e){
            //parse failed
            throw 'Unable to parse input string as JSON; input:' + init_json + '; Error: ' + e;
        }
    } else {
        //consider input as Object. if not - default values still be there
        init_obj = init_json;
    }
    //Let's try to find useful props..
    for (prop in init_obj) {
        // consider a prop useful if current obj has own prop with same name(created by default).
        // functions are stored in prototype, so they won't be in list
        if (init_obj.hasOwnProperty(prop) && this.hasOwnProperty(prop)) {
            //check that types match
            if (typeof this[prop] === typeof init_obj[prop]) {
                if (typeof this[prop] === "object") {
                    //Call Constructor for Object types
                    this[prop] = new this[prop].__proto__.constructor(init_obj[prop]);
                } else {
                    // Others - just update values...
                    this[prop] = init_obj[prop];
                }
            }
            else {
                //type mismatch
                console.log('Unable to init property "' + prop + '" with value "' + init_obj[prop] + '"; Type should be: ' + this[prop].__proto__.constructor.name);
            }
            // In some cases initial JSON has links to other objects
            // Should search fo exisiting itEmployees ot smth if it is passed as input string to Employee prop
            // Another example: new Department, but head is an existing manager. It should be linked, but not created again...
        }
    }
}

Department.prototype.addEmployee = function(Emp){
    console.log('I plan to add Employee "' + Emp + '" to Dept "' + this.name + '"');
    this.employees.push(Emp);
};
Department.prototype.removeEmployee = function(Emp){
    console.log('I plan to remove Employee "' + Emp + '" from Dept "' + this.name + '"')
    //splice should by used probably...
};

Department.prototype.toString = function(){
    var strResult = this.__proto__.constructor.name + ': {';
    for(prop in this){
        if(this.hasOwnProperty(prop)){
            strResult += ' "' + prop + '": ' + this[prop].toString() + ',';
        }
    }
    strResult = strResult.substr(0,strResult.length-1) + ' }';
    return strResult;
};

//Sample data...
/*var init_json = '{ "name": "Кабинет Васи", "employees": 35, "head": {"name":"Вася"}, "friends": [0,1,2,3] }';
document.write("a = new Department("+ init_json + ");");
document.write('<br>');
/*
var a = new Department(init_json);
document.write(a.toString());
*/