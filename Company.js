/**
 * Created by Vadon on 30.01.2016.
 */
function Company(init_val) {
    setDefaults.call(this);                         //set default values to 'this'
    var name;
    var init_obj = parseCompany(init_val);          //if input is a string, parse it
    loadDepartments.call(this,init_obj);            //create departments from input value
    loadTasks.call(this,init_obj);                  //create tasks
    //define setter and getter on name property
    Object.defineProperty(this, "name", {set: function(val){checkString(val); name = val;}, get: function(){return name;}});
    this.name = init_obj.name;                      //setter is used here

    //only definitions left
    function setDefaults(){
        name = 'Tinkoff';                           //name stored in closure, not in 'this'
        this.departments = [];
        this.tasks = {createTask: function(){}};
    }

    function parseCompany(val){
        if (typeof val == 'string') {               //identify input data type - JSON/Object
            console.log('String detected. Parsing as JSON');
            try{
                return JSON.parse(val);
            } catch(e){
                //parse failed
                throw 'Unable to parse input string as JSON; input:' + val + '; Error: ' + e;
            }
        } else {
            //consider input as Object. if not - default values still be there
            return val;
        }
    }
    function loadDepartments(input){
        if(input.departments){
            try {
                for (var Num in input.departments) {
                    this.departments.push(new Department(input.departments[Num]))
                }
            }catch(e){
                throw "Error loading departments: " + e.toString()
            }
        }
    }

    function loadTasks(input){
        if(input.tasks){
            try {
                for (var Num in input.tasks) {
                    this.tasks.createTask(input.tasks[Num]);
                }
            }catch(e){
                throw "Error loading tasks: " + e.toString()
            }
        }
    }

    function checkString(val){
        if(val && val.match(/[^a-zA-Zа-яА-Я0-9_ "'()&#№+-]/)){
            throw "Некорректный формат: " + val;
        }
    }
}

Company.prototype.addDepartment = function(Dep){
    if(Dep.__proto__.constructor.name == 'Department'){
        console.log('I plan to add Department "' + Dep.name + '" to Company "' + this.name + '"');
        this.departments.push(Dep);
    }else{
        throw "Invalid input Object - should be 'Department'"
    }
};
Company.prototype.removeDepartment = function(Dep){
    var name = Dep.__proto__.constructor.name == 'Department' ? Dep.name : Dep.toString();
    console.log('I plan to remove Department "' + name + '" from Company "' + this.name + '"');
    //TODO: splice should by used probably...
    for(j in this.departments){
        if(this.departments[j].name == name){
            this.departments = this.departments.splice(j+1,1);            //TODO: check, may be 'j+1' should be used...
            break;
        }
    }
};
Company.prototype.toString = function(){
    var strResult = this.__proto__.constructor.name + ': {';
    for(prop in this){
        if(this.hasOwnProperty(prop)){
            strResult += ' "' + prop + '": ' + this[prop].toString() + ',';
        }
    }
    strResult = strResult.substr(0,strResult.length-1) + ' }';
    return strResult;
};

/*
 a = new Company('{"name": "Вася & Петя","head": {"name":"Вася"},"departments":[{"name": "Кабинет Васи", "employees": [{"name":"Петя завхоз"}], "head": {"name":"Вася"}},{"name": "каморка Пети", "employees": [], "head": {"name":"Петя"}}]}');

 a.addDepartment(new Department('{ "name": "Служба доставки Коли", "employees": [], "head": {"name":"Коля"}}'))
 */