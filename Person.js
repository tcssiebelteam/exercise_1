function Person(init_json){
	var init_obj = JSON.parse(init_json);
	if (!("name" in init_obj && "surname" in init_obj))
		throw new Error("Фамилия и имя обязательны");
	var name = init_obj.name;
	var surname = init_obj.surname;
	var dob = init_obj.dob;	
	var phone = init_obj.phone;
	var email = init_obj.email;
	
	function check(name, opty){
		if (typeof name != 'string')
			throw new Error("Значение для свойства '" + opty + "' должно быть строчкой");
		if (name.length > 50)
			throw new Error("Слишком длинное значения для свойства '" + opty + "'");
	}
	function checkDate(d){
		var rDate = /[0-3]\d\.[0-1]\d\.\d{4}/;
		if (!d.match(rDate))
			throw new Error("Некорректный формат даты");
	}
	
	check(name, "Имя");
	check(surname, "Фамилия");
	checkDate(dob);
		
	var rPhone = /\+\d\(\d{3}\)\d{3}-\d{2}-\d{2}/;
	if (!phone.match(rPhone))
		throw new Error("Телефон должен быть в формате +d(ddd)ddd-dd-dd");
	var rEmail = /.+@.+\..+/;
	if (!email.match(rEmail))
		throw new Error("Некорректный формат почтового ящика");

	dob = new Date(dob.substr(6,4), dob.substr(3,2)-1, dob.substr(0,2));
	this.phone = phone;
	this.email = email;
	// setter and getter
	Object.defineProperty(this, "name", {set: function(val){check(val, "Имя"); name = val;}, get: function(){return name;}});
	Object.defineProperty(this, "surname", {set: function(val){check(val, "Фамилия"); surname = val;}, get: function(){return surname;}});
	Object.defineProperty(this, "dob", {
		set: function(val){
			checkDate(val); 
			dob = new Date(val.substr(6,4), val.substr(3,2)-1, val.substr(0,2));
		}
		, get: function(){
			var m = dob.getMonth() + 1;
			if (m < 10)
				m = "0" + m;
			return dob.getDate() + "." + m + "." + dob.getFullYear();
		}
	});
};
Person.prototype.getAge = function(){
		var curDate = new Date();
		var dob = this.dob;
		dob = new Date(dob.substr(6,4), dob.substr(3,2)-1, dob.substr(0,2));
		var age = curDate.getFullYear() - dob.getFullYear();
		if (curDate.setFullYear(2000) < dob.setFullYear(2000))
			age--;
		return age;
};
Person.prototype.getFullName = function(){
		return this.name + " " + this.surname;
};
var p = new Person('{ "name": "Петя", "surname": "Петров", "dob": "28.01.1990", "phone": "+7(288)212-22-33", "email": "dd@mail.ru"}');